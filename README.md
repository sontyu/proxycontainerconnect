# ProxyContainerConnect
社内proxy環境でdockerコンテナ間の通信がうまくいかなかったため、備忘録代わりにproxy環境での通信環境の例を残しておきます。

## 事象
社内proxy環境でdockerコンテナ間でhttp通信を行おうとしたが通信が通らなかった。

### 原因
社内ネットワーク環境でコンテナを立ち上げたため、社内プロキシを利用した環境になっていた。
そのため、http通信をしたところ社内プロキシサーバーを経由して通信を行っていたため、通信ができなかった。

### 事象発覚
下記のコマンドが通らなかった。
```bash 
$ docker exec -ti web1 curl http://web2/index.html
```


### 事象解明方法
接続の詳細を出力したことで原因が発覚した。

```bash
$ docker exec -ti web1 curl http://web2/index.html -v

*Uses proxy env variable http_proxy == '<address_to_proxy>'
.......
```
これによってプロキシサーバー経由でweb2コンテナにアクセスしようとしていたことが発覚。

ちなみに出力は何もなし。

### 対応策
[docker-compose.yml](docker-compose.yml)で下記の設定を行った。

- networks, ipaddress, コンテナ名を明示的に固定
- no_proxyに固定したコンテナ名とipaddressを追加

### 実行結果
```bash
$ docker exec -ti web1 curl http://web2/index.html -v

*Uses proxy env variable no_proxy == "127.0.0.1,localhost,web1,192.168.1.2,web2,192.168.1.3"
.......
<正常な出力結果>
```

